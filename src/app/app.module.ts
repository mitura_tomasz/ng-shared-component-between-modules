import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BasketballTeamModule } from './components/basketball-team/basketball-team.module';
import { FootballTeamModule } from './components/football-team/football-team.module';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BasketballTeamModule,
    FootballTeamModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
