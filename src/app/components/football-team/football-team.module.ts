import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayerModule } from '../player/player.module';

import { FootballTeamRoutingModule } from './football-team-routing.module';
import { FootballTeamComponent } from './football-team.component';

@NgModule({
  imports: [
    CommonModule,
    FootballTeamRoutingModule,
    PlayerModule
  ],
  declarations: [FootballTeamComponent],
  exports: [
    FootballTeamComponent
  ]
})
export class FootballTeamModule { }
