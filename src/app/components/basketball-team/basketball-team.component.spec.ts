import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasketballTeamComponent } from './basketball-team.component';

describe('BasketballTeamComponent', () => {
  let component: BasketballTeamComponent;
  let fixture: ComponentFixture<BasketballTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasketballTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketballTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
