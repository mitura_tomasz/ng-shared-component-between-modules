import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayerModule } from '../player/player.module';

import { BasketballTeamRoutingModule } from './basketball-team-routing.module';
import { BasketballTeamComponent } from './basketball-team.component';

@NgModule({
  imports: [
    CommonModule,
    BasketballTeamRoutingModule,
    PlayerModule
  ],
  declarations: [BasketballTeamComponent],
  exports: [
    BasketballTeamComponent
  ]
})
export class BasketballTeamModule { }
